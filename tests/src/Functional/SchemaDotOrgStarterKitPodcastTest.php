<?php

declare(strict_types=1);

namespace Drupal\Tests\schemadotorg_starterkit_podcast\Functional;

use Drupal\Tests\schemadotorg\Functional\SchemaDotOrgBrowserTestBase;

/**
 * Tests the functionality of the Schema.org Starter Kit: Podcast module.
 *
 * @group schemadotorg_starterkit_podcast
 */
class SchemaDotOrgStarterKitPodcastTest extends SchemaDotOrgBrowserTestBase {

  // phpcs:disable
  /**
   * Disabled config schema checking.
   */
  protected $strictConfigSchema = FALSE;
  // phpcs:enable

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['shortcut', 'schemadotorg_starterkit_podcast', 'schemadotorg_jsonld'];

  /**
   * Test Schema.org Starter Kit: Podcast.
   */
  public function testPodcast(): void {
    /** @var \Drupal\node\NodeStorage $node_storage */
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');

    // Check forcing installation of optional config.
    $this->assertNotNull($this->config('auto_entitylabel.settings.node.podcast_episode'));

    // Check removing EVA from the podcast series view displays.
    $this->assertNull(
      $this->config('core.entity_view_display.node.podcast_series.default')->get('hidden.podcast_episodes_entity_view_1')
    );
    $this->assertTrue(
      $this->config('core.entity_view_display.node.podcast_series.teaser')->get('hidden.podcast_episodes_entity_view_1')
    );

    // Check adding Podcasts default shortcut.
    /** @var \Drupal\shortcut\ShortcutSetStorageInterface $shortcut_storage */
    $shortcut_storage = \Drupal::entityTypeManager()->getStorage('shortcut');
    $this->assertNotEmpty($shortcut_storage->loadByProperties(['title' => 'Podcasts']));

    // Login as root user.
    $this->drupalLogin($this->rootUser);

    /** @var \Drupal\node\NodeInterface $series_node */
    $series_node = $this->drupalCreateNode([
      'type' => 'podcast_series',
      'title' => 'Podcast series',
    ]);
    $series_node->save();

    /** @var \Drupal\node\NodeInterface $episode_node */
    $episode_node = $this->drupalCreateNode([
      'type' => 'podcast_episode',
      'schema_name' => ['value' => 'Podcast episode'],
      'schema_part_of_series' => ['target_id' => $series_node->id()],
    ]);
    $episode_node->save();

    // Check updating a Podcast Series changed time when a new episode is added.
    // @see schemadotorg_starterkit_podcast_node_presave()
    $changed = $series_node->getChangedTime();
    $node_storage->resetCache();
    $series_node = $node_storage->load($series_node->id());
    $this->assertLessThan($series_node->getChangedTime(), $changed);

    // Check setting a new podcast episode's episode number.
    // @see _schemadotorg_starterkit_podcast_set_episode_number()
    $this->assertEquals(1, $episode_node->schema_episode_number->value);
    $another_episode_node = $this->drupalCreateNode([
      'type' => 'podcast_episode',
      'schema_name' => ['value' => 'Another podcast episode'],
      'schema_part_of_series' => ['target_id' => $series_node->id()],
    ]);
    $another_episode_node->save();
    $this->assertEquals(2, $another_episode_node->schema_episode_number->value);

    // Check using computed podcast episode's label in JSON-LD.
    // @see schemadotorg_starterkit_podcast_schemadotorg_jsonld_schema_type_entity_alter()
    /** @var \Drupal\schemadotorg_jsonld\SchemaDotOrgJsonLdBuilderInterface $jsonld_builder */
    $jsonld_builder = $this->container->get('schemadotorg_jsonld.builder');
    $jsonld = $jsonld_builder->buildEntity($episode_node);
    $this->assertEquals('Podcast series #1 - Podcast episode', $jsonld['name']);
  }

}
