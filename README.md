Table of contents
-----------------

* Introduction
* Features
* Schema.org types
* References


Introduction
------------

The **Schema.org Blueprints Starter Kit: Podcast module** provides
a Schema.org PodcastSeries and PodcastEpisode types with views and RSS feeds.


Features
--------

- Creates a /podcasts/{series} view.
- Creates a /podcasts/{series}/rss.xml feed.
- Add paths aliases for /podcasts/{series}/{episode}
- Attaches episodes to a podcasts full display.
- Rewrites https://schema.org/samesAs to only collect external URLs.
- Updates a Podcast Series changed time when a new episode is added.
- Sets a new podcast episode's episode number.
- Automatically sets the podcast episode's title to include the podcast series
  name and episode number.


Schema.org types
----------------

Below are the Schema.org types created by this starter kit.

- **Person** (node:Person)  
  A person (alive, dead, undead, or fictional).  
  <https://schema.org/Person>

- **Podcast Series** (node:PodcastSeries)  
  A podcast is an episodic series of digital audio or video files which a user can download and listen to.  
  <https://schema.org/PodcastSeries>

- **Podcast Episode** (node:PodcastEpisode)  
  A single episode of a podcast series.  
  <https://schema.org/PodcastEpisode>

References
----------

Podcast/RSS examples

- https://www.talkingdrupal.com/
- https://www.lullabot.com/podcasts
